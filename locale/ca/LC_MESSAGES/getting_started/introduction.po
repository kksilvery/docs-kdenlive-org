# Translation of docs_kdenlive_org_getting_started___introduction.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-02-25 16:05+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../getting_started/introduction.rst:1
msgid "Introduction to Kdenlive video editor"
msgstr "Introducció a l'editor de vídeo Kdenlive"

#: ../../getting_started/introduction.rst:1
msgid ""
"KDE, Kdenlive, Introduction, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, introducció, documentació, manual d'usuari, editor de vídeo, "
"codi lliure, lliure, aprendre, fàcil"

#: ../../getting_started/introduction.rst:29
msgid "Introduction"
msgstr "Introducció"

#: ../../getting_started/introduction.rst:32
msgid "Contents"
msgstr "Contingut"

#: ../../getting_started/introduction.rst:34
msgid ""
"**Kdenlive** is an acronym for `KDE <http://www.kde.org>`_ **N**\\ on-\\ "
"**Li**\\ near **V**\\ ideo **E**\\ ditor. It is a free software (`GPL "
"licensed <http://www.fsf.org/licensing/licenses/gpl.html>`_) primarily aimed "
"at the Linux platform, but it also works on BSD [1]_  as it relies only on "
"portable components (`Qt <https://www.qt.io/>`_ and `MLT <http://www."
"mltframework.org/>`_ framework). Windows versions are also available, with "
"some drawbacks. See :ref:`windows_issues` for more information. A port on "
"MacOS is `currently in progress <https://invent.kde.org/multimedia/"
"kdenlive/-/issues/993>`_."
msgstr ""
"El **Kdenlive** és un acrònim per a Editor de vídeo no lineal («`KDE "
"<https://www.kde.org>`_ **N**\\ on-\\ **Li**\\ near **V**\\ ideo **E**\\ "
"ditor»). És programari lliure (`llicència GPL <http://www.fsf.org/licensing/"
"licenses/gpl.html>`_) adreçat principalment a la plataforma Linux, però "
"també funciona a BSD [1]_, ja que només es basa en components portables (les "
"`Qt <https://www.qt.io/>`_ i l'entorn de treball `MLT <https://www."
"mltframework.org/>`_). També hi ha disponibles versions de Windows, amb "
"alguns inconvenients. Per a obtenir més informació, vegeu els :ref:"
"`windows_issues`. Actualment hi ha `una adaptació a MacOS en curs <https://"
"invent.kde.org/multimedia/kdenlive/-/issues/993>`_."

#: ../../getting_started/introduction.rst:38
msgid ""
"*Non-linear video editing* is much more powerful than beginners' (linear) "
"editors, hence it requires a bit more organization before starting. However, "
"it is not reserved to specialists and can be used for small personal "
"projects."
msgstr ""
"L'*edició de vídeo no lineal* és molt més potent que els editors (lineals) "
"per a principiants, per tant, requereix una mica més d'organització abans de "
"començar. No obstant això, no està reservada a especialistes i es pot "
"utilitzar per a petits projectes personals."

#: ../../getting_started/introduction.rst:41
msgid ""
"Through the MLT framework, **Kdenlive** integrates many plugin effects for "
"video and sound processing or creation. Furthermore **Kdenlive** brings a "
"powerful titling tool, a subtitling feature with automatic speech to text "
"conversion, and can then be used as a complete studio for video creation."
msgstr ""
"A través de l'entorn de treball MLT, el **Kdenlive** integra molts efectes "
"de connectors per al processament o creació de vídeo i so. A més, el "
"**Kdenlive** ofereix una potent eina de títols, una característica de "
"subtitulació amb conversió automàtica de veu a text, i després es pot "
"utilitzar com a un complet estudi per a la creació de vídeo."

#: ../../getting_started/introduction.rst:47
msgid "Video editing features"
msgstr "Característiques de l'edició de vídeo"

# skip-rule: ff-facilities
#: ../../getting_started/introduction.rst:54
msgid ""
"Multitrack edition with a timeline and virtually unlimited number of video "
"and audio tracks, plus facilities for splitting audio and video from a clip "
"in multiple tracks"
msgstr ""
"Edició multipista amb una línia de temps i un nombre pràcticament il·limitat "
"de pistes de vídeo i àudio, a més de facilitats per a dividir l'àudio i el "
"vídeo d'un clip en múltiples pistes"

#: ../../getting_started/introduction.rst:57
msgid ""
"Non-blocking rendering. You can keep working on a project at the same time a "
"project is being transformed into a video file"
msgstr ""
"Renderització sense bloqueigs. Podeu seguir treballant en un projecte alhora "
"que se'n transforma un altre en un fitxer de vídeo"

# skip-rule: kct-wip
#: ../../getting_started/introduction.rst:60
msgid ""
"Effects and transitions can be used with ease, and you can even create some "
"wipe transitions of your own!"
msgstr ""
"Els efectes i les transicions es poden utilitzar amb facilitat, fins i tot "
"es poden crear algunes transicions de cortineta!"

#: ../../getting_started/introduction.rst:63
msgid ""
"Simple tools for easy creation of color clips, text clips and image clips"
msgstr ""
"Eines senzilles per a crear amb facilitat clips de color, clips de text i "
"clips d'imatge"

#: ../../getting_started/introduction.rst:66
msgid ""
"Automatic :ref:`clips` creation from pictures directories, with crossfade "
"transitions among the images"
msgstr ""
"Creació automàtica de :ref:`clips` a partir de directoris d'imatges, amb "
"transicions de fusió creuada entre les imatges"

#: ../../getting_started/introduction.rst:69
msgid "Configurable keyboard shortcuts and interface layouts"
msgstr "Dreceres de teclat i disposicions de la interfície configurables"

#: ../../getting_started/introduction.rst:72
msgid "and much more!"
msgstr "i molt més!"

#: ../../getting_started/introduction.rst:75
msgid "Berkeley Software Distribution"
msgstr "Distribució de programari de Berkeley"
