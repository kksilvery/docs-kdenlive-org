# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-05-08 16:36+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/menu/settings_menu/download_new_wipes.rst:14
msgid "Download New Wipes"
msgstr "새 닦아내기 다운로드"

#: ../../user_interface/menu/settings_menu/download_new_wipes.rst:17
msgid "Contents"
msgstr "목차"

#: ../../user_interface/menu/settings_menu/download_new_wipes.rst:19
msgid ""
"This feature allows you to download and install files that can be used as "
"Wipe files in the :ref:`wipe` transition.  These files are greyscale images "
"in the pgm format."
msgstr ""

#: ../../user_interface/menu/settings_menu/download_new_wipes.rst:21
msgid ""
"If you have your own wipe files that you would like to share with the "
"community you can upload them to `store.kde.org <https://store.kde.org/"
"browse/cat/185/>`_ which should make them available from this *Download New "
"Wipes* function."
msgstr ""
