# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 11:54+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/effect_groups/artistic/scratchlines.rst:14
msgid "Scratchlines"
msgstr "Replinjer"

#: ../../effects_and_compositions/effect_groups/artistic/scratchlines.rst:16
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/effect_groups/artistic/scratchlines.rst:18
msgid ""
"This is the `Lines <https://www.mltframework.org/plugins/FilterLines/>`_ MLT "
"filter."
msgstr ""
"Det här är MLT-filtret `Linjer <https://www.mltframework.org/plugins/"
"FilterLines/>`_."

#: ../../effects_and_compositions/effect_groups/artistic/scratchlines.rst:20
msgid "Adds scratch lines over the picture."
msgstr "Lägger till replinjer över bilden."

#: ../../effects_and_compositions/effect_groups/artistic/scratchlines.rst:22
msgid "https://youtu.be/F8gZhHDQQBE"
msgstr "https://youtu.be/F8gZhHDQQBE"

#: ../../effects_and_compositions/effect_groups/artistic/scratchlines.rst:24
msgid "https://youtu.be/3laMxZBwqqY"
msgstr "https://youtu.be/3laMxZBwqqY"
