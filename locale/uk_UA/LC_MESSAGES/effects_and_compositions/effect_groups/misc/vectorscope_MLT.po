# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-03 19:04+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:14
msgid "Analysis and Data - Vectorscope"
msgstr "Аналіз і дані — Векторскоп"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:18
msgid ""
"This is the `Frei0r vectorscope <https://www.mltframework.org/plugins/"
"FilterFrei0r-vectorscope/>`_ MLT filter."
msgstr ""
"Це фільтр MLT `frei0r.vectorscope <https://www.mltframework.org/plugins/"
"FilterFrei0r-vectorscope/>`_."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:20
msgid "Displays the vectorscope of the video-data."
msgstr "Показ векторскопа відеоданих."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:22
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""
"У версії 17.04 пункт цього ефекту розташовано у категорії :ref:"
"`analysis_and_data` меню «Ефекти»."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:24
msgid ""
"It is recommended to use the vectorscope from :ref:`vectorscope`, because "
"the effect *Analysis and Data - Vectorscope* is not correct - it uses a "
"graticule from an analog NTSC vectorscope, but equations from digital video."
msgstr ""
"Рекомендуємо скористатися векторскопом з ефекту :ref:`vectorscope`. Оскільки "
"ефект це ефект «Аналіз і дані» — назва «Векторскоп» не є коректною — він "
"використовує сіткою з аналогового векторскопа NTSC, але рівняння з цифрового "
"відео."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:26
msgid "https://youtu.be/2ybBzDEjdRo"
msgstr "https://youtu.be/2ybBzDEjdRo"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:28
msgid "https://youtu.be/O1hbS6VZh_s"
msgstr "https://youtu.be/O1hbS6VZh_s"
