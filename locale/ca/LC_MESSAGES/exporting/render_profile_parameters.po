# Translation of docs_kdenlive_org_exporting___render_profile_parameters.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-02 00:38+0000\n"
"PO-Revision-Date: 2022-05-02 10:54+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../exporting/render_profile_parameters.rst:1
msgid "The Kdenlive User Manual"
msgstr "El manual d'usuari del Kdenlive"

#: ../../exporting/render_profile_parameters.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, learn, render, render profile, render parameter"
msgstr ""
"KDE, Kdenlive, documentació, manual d'usuari, editor de vídeo, codi lliure, "
"lliure, ajuda, aprendre, renderitzar, perfil de renderització, paràmetre de "
"renderització"

#: ../../exporting/render_profile_parameters.rst:21
msgid "Render Profile Parameters"
msgstr "Paràmetres del perfil de renderització"

#: ../../exporting/render_profile_parameters.rst:24
msgid "Contents"
msgstr "Contingut"

#: ../../exporting/render_profile_parameters.rst:27
#: ../../exporting/render_profile_parameters.rst:92
msgid "Render Profile Parameters - How to read them"
msgstr "Paràmetres del perfil de renderització -com llegir-los-"

#: ../../exporting/render_profile_parameters.rst:44
msgid ""
"Kdenlive now makes use of \"property presets\" delivered by the *melt* "
"project (see `melt doco <https://www.mltframework.org/docs/presets/>`_). "
"These presets are referenced by the *properties=<preset>* syntax. In the "
"example illustrated, the render profile is referencing *lossless/H.264*. "
"This refers to a property preset found in file H.264 found on the system at :"
"file:`/usr/share/mlt/presets/consumer/avformat/lossless`."
msgstr ""
"El Kdenlive ara fa ús dels «predefinits de propietats» oferts pel projecte "
"*melt* (vegeu `melt doco <https://www.mltframework.org/docs/presets/>`_). A "
"aquests predefinits s'hi fa referència amb la sintaxi *properties=<preset>*. "
"A l'exemple il·lustrat, el perfil de renderització fa referència a *lossless/"
"H.264*. Això fa referència a un predefinit de propietats que es troba en el "
"fitxer H.264 que en el sistema es troba a :file:`/usr/share/mlt/presets/"
"consumer/avformat/lossless`."

#: ../../exporting/render_profile_parameters.rst:46
msgid ""
"All the *<presets>* referenced in the render settings in Kdenlive will be "
"referring to presets found at :file:`/usr/share/mlt/presets/consumer/"
"avformat/` (on a default install). Note that you reference presets found in "
"subdirectories of this folder using a :file:`<dirname>/<profile>` syntax as "
"shown in the example above."
msgstr ""
"Tots els *<predefinits>* a què es fa referència a les opcions de "
"configuració de la renderització del Kdenlive es referiran a predefinits que "
"es troben a :file:`/usr/share/mlt/presets/consumer/avformat/` (en una "
"instal·lació predeterminada). Cal tenir en compte que s'ha de fer referència "
"als predefinits que es troben als subdirectoris en aquesta carpeta "
"mitjançant una sintaxi <nom_directori>/<perfil>, tal com es mostra a "
"l'exemple anterior."

#: ../../exporting/render_profile_parameters.rst:55
msgid ""
"The preset files found at :file:`/usr/share/mlt/presets/consumer/avformat/` "
"are simple text files that contain the *melt* parameters that define the "
"rendering. An example is shown below. These are the same parameters that "
"were used in earlier versions of Kdenlive – see next section for how to read "
"those."
msgstr ""
"Els fitxers predefinits que es troben a :file:`/usr/share/mlt/presets/"
"consumer/avformat/` són fitxers de text senzills que contenen els paràmetres "
"de *melt* que defineixen la renderització. A continuació es mostra un "
"exemple. Aquests són els mateixos paràmetres que s'utilitzaven en versions "
"anteriors del Kdenlive. Per a saber com llegir-los, vegeu la secció següent."

#: ../../exporting/render_profile_parameters.rst:57
msgid "Contents of lossless/H.264:"
msgstr "Contingut de lossless/H.264:"

#: ../../exporting/render_profile_parameters.rst:79
msgid "Scanning Dropdown"
msgstr "Llista desplegable Escaneig"

#: ../../exporting/render_profile_parameters.rst:85
msgid ""
"This option controls the frame scanning setting the rendered file will have. "
"Options are *Force Progressive*, *Force Interlaced* and *Auto*."
msgstr ""
"Aquesta opció controla la configuració de l'exploració de fotogrames que "
"tindrà el fitxer renderitzat. Les opcions són la *Força l'escombrat "
"progressiu*, *Força l'entrellaçat* i *Automàtica*."

#: ../../exporting/render_profile_parameters.rst:88
msgid ""
":menuselection:`Auto` causes the rendered file to take the scanning settings "
"that are defined in the :ref:`project_settings`. Use the other options to "
"override the setting defined in the project settings."
msgstr ""
":menuselection:`Automàtica` fa que el fitxer renderitzat prengui les opcions "
"de configuració de l'exploració que es defineixin en el :ref:"
"`project_settings`. Utilitzeu les altres opcions per a superposar la "
"configuració definida a la configuració del projecte."

#: ../../exporting/render_profile_parameters.rst:96
msgid "|outdated|"
msgstr "|outdated|"

#: ../../exporting/render_profile_parameters.rst:98
msgid ""
"The parameters that go into a render profile derive from the **ffmpeg** "
"program."
msgstr ""
"Els paràmetres que van dins d'un perfil de renderització deriven del "
"programa **ffmpeg**."

#: ../../exporting/render_profile_parameters.rst:100
msgid ""
"This is a worked example to show how you can understand what these "
"parameters mean using the **ffmpeg** documentation."
msgstr ""
"Aquest és un exemple treballat per a mostrar com podeu entendre què "
"signifiquen aquests paràmetres mitjançant la documentació de **ffmpeg**."

#: ../../exporting/render_profile_parameters.rst:102
msgid "In the example above the parameters are:"
msgstr "A l'exemple anterior els paràmetres són:"

# skip-rule: t-acc_obe
#: ../../exporting/render_profile_parameters.rst:124
msgid ""
"Looking up the `ffmpeg help <https://linux.die.net/man/1/ffmpeg>`_ "
"translates these parameters as shown below."
msgstr ""
"Si cerqueu a l'`ajuda de ffmpeg <https://linux.die.net/man/1/ffmpeg>`_, "
"aquests paràmetres es tradueixen tal com es mostra a continuació."

#: ../../exporting/render_profile_parameters.rst:126
msgid "Main option is:"
msgstr "L'opció principal és:"

#: ../../exporting/render_profile_parameters.rst:132
msgid "Video options are:"
msgstr "Les opcions de vídeo són:"

#: ../../exporting/render_profile_parameters.rst:144
msgid "Audio options are:"
msgstr "Les opcions d'àudio són:"

#: ../../exporting/render_profile_parameters.rst:152
msgid "The AVCodecContext AVOptions include:"
msgstr "Les AVOptions de l'AVCodecContext inclouen:"

#: ../../exporting/render_profile_parameters.rst:161
msgid ""
"So all the render profile options are documented here in the **ffmpeg** "
"documentation."
msgstr ""
"Per tant, totes les opcions del perfil de renderització documentades aquí "
"també ho estan a la documentació de **ffmpeg**."

# skip-rule: t-acc_obe
#: ../../exporting/render_profile_parameters.rst:163
msgid ""
"See also `MLT doco <https://www.mltframework.org/docs/presets/>`_ on "
"ConsumerAvFormat."
msgstr ""
"Vegeu també `MLT doco <https://www.mltframework.org/plugins/ConsumerAvformat/"
">`_ a ConsumerAvFormat."

# skip-rule: t-acc_obe
#: ../../exporting/render_profile_parameters.rst:165
msgid ""
"See also `HOWTO Produce 4k and 2K videos, YouTube compatible <https://forum."
"kde.org/viewtopic.php?f=272&amp;t=124869#p329129>`_."
msgstr ""
"Vegeu també `Com produir vídeos en 4k i 2K, compatibles amb YouTube <https://"
"forum.kde.org/viewtopic.php?f=272&amp;t=124869#p329129>`_."
