# Translation of docs_kdenlive_org_importing_and_assets_management___projects_and_files___archiving.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-17 00:38+0000\n"
"PO-Revision-Date: 2022-04-17 11:12+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:1
msgid "The Kdenlive User Manual"
msgstr "El manual d'usuari del Kdenlive"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, archive, archiving"
msgstr ""
"KDE, Kdenlive, documentació, manual d'usuari, editor de vídeo, codi lliure, "
"lliure, arxiu, arxivament"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:20
msgid "Archiving"
msgstr "Arxivament"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:26
msgid ""
"The Archiving feature (:menuselection:`Project --> Archive Project`, see :"
"ref:`project_menu`) in **Kdenlive** allows you to copy all files required by "
"the project (images, video clips, project files,...) to a folder, and "
"alternatively to compress the whole into a tar.gz or a zip file."
msgstr ""
"La característica d'Arxivament (:menuselection:`Projecte --> Arxiva el "
"projecte`, vegeu :ref:`project_menu`) al **Kdenlive** permet copiar tots els "
"fitxers requerits pel projecte (imatges, clips de vídeo, fitxers de "
"projecte,...) a una carpeta, i alternativament comprimir el conjunt en un "
"arxiu «tar.gz» o un arxiu «zip»."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:28
msgid ""
"Archiving changes the project file to update the path of video clips to the "
"archived versions."
msgstr ""
"Arxivament canvia el fitxer del projecte per actualitzar el camí dels clips "
"de vídeo a les versions arxivades."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:30
msgid ""
"This can be useful if you finished working on a project and want to keep a "
"copy of it, or if you want to move a project from one computer to another."
msgstr ""
"Això pot ser útil si acabeu de treballar en un projecte i voleu mantenir una "
"còpia d'ell, o si voleu moure un projecte d'un ordinador a un altre."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:38
msgid ""
"The resulting tar.gz or zip file can be opened directly in **Kdenlive** "
"with :menuselection:`File --> Open`, then switch file ending to :guilabel:"
"`Archived Project`."
msgstr ""
"El fitxer «tar.gz» resultant es pot obrir directament al **Kdenlive**. Amb :"
"menuselection:`Fitxer --> Obre`, i canviar el final del fitxer a :guilabel:"
"`Projecte arxivat`."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:46
msgid ""
"Kdenlive will uncompress it to a location you specify before opening it."
msgstr ""
"El Kdenlive el descomprimirà a una ubicació que especifiqueu abans d'obrir-"
"lo."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:55
msgid ""
"If you have archived the project with the option :guilabel:`Archive only "
"timeline clips`, **Kdenlive** ask what it should do with the not archived "
"clips."
msgstr ""
"Si heu arxivat el projecte amb l'opció :guilabel:`Arxiva només els clips de "
"la línia de temps`, el **Kdenlive** demanarà què hauria de fer amb els clips "
"no arxivats."

#~ msgid "Contents"
#~ msgstr "Contingut"
