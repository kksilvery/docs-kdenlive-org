# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-01 00:38+0000\n"
"PO-Revision-Date: 2022-03-31 23:49+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:10
msgid "Motion Tracker"
msgstr "Volger van beweging"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:13
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:16
msgid "What is Motion Tracking?"
msgstr "Wat is volgen van beweging?"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:18
msgid ""
"Motion tracking is the process of locating a moving object across time. "
"Kdenlive uses `OpenCV (Open Source Computer Vision Library) <https://opencv."
"org/about/>`_ for motion detection."
msgstr ""
"Volgen van beweging is het proces van localiseren van een bewegend object in "
"de tijd. Kdenlive gebruikt `OpenCV (Open Source Computer Vision Library) "
"<https://opencv.org/about/>`_ voor detectie van beweging."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:20
msgid "-- `Wikipedia <https://en.wikipedia.org/wiki/Video_tracking>`_"
msgstr "-- `Wikipedia <https://en.wikipedia.org/wiki/Video_tracking>`_"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:26
msgid "How to track a region of a video?"
msgstr "Hoe een gebied van een video te volgen?"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:28
msgid "The basic workflow for tracking a region consists of:"
msgstr "De basis werkmethode voor volgen van een gebied bestaat uit:"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:30
msgid "Select the desired region to track on the Project Monitor."
msgstr "Selecteer het gewenste te volgen gebied op de Projectmonitor."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:31
msgid "Choose a tracking algorithm."
msgstr "Kies een volgalgoritme."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:32
msgid "Click on the Analyse button."
msgstr "Klik op de knop Analyseren."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:41
msgid ""
"When Analyse is done you can export the keyframes to the clipboard by click "
"on |application-menu| and choose :menuselection:`Copy keyframes to "
"clipboard`. See: :ref:`exchange_keyframes_across_effects`"
msgstr ""
"Na Analyseren kunt u de keyframes exporteren naar het klembord door te "
"klikken op |application-menu| en :menuselection:`Keyframes kopiëren naar "
"klembord` te kiezen. Zie: :ref:`exchange_keyframes_across_effects`"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:46
msgid "Tracking algorithms"
msgstr "Volgalgoritmes"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:48
msgid "KCF"
msgstr "KCF"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:50
msgid "**Kernelized Correlation Filters**"
msgstr "**Kernelized Correlation Filters**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:52
msgid ""
"**Pros:** Accuracy and speed are both better than MIL and it reports "
"tracking failure better than MIL."
msgstr ""
"**Voordelen:** Nauwkeurigheid en snelheid zijn beiden beter dan MIL en het "
"rapporteert mislukken van volgen beter dan MIL."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:54
msgid "**Cons:** Does not recover from full occlusion."
msgstr "**Nadeel:** Herstelt niet uit volledige bedekking."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:57
msgid "CSRT"
msgstr "CSRT"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:59
msgid ""
"In the Discriminative Correlation Filter with Channel and Spatial "
"Reliability (DCF-CSR), we use the spatial reliability map for adjusting the "
"filter support to the part of the selected region from the frame for "
"tracking. This ensures enlarging and localization of the selected region and "
"improved tracking of the non-rectangular regions or objects. It uses only 2 "
"standard features (HoGs and Colornames). It also operates at a comparatively "
"lower fps (25 fps) but gives higher accuracy for object tracking."
msgstr ""
"In het \"Discriminative Correlation Filter with Channel and Spatial "
"Reliability (DCF-CSR)\", gebruiken we de ruimtelijke betrouwbaarheidskaart "
"voor aanpassen van de filterondersteuning tot het deel van het geselecteerde "
"gebied uit het frame voor volgen. Dit verzekert vergroten en lokaliseren van "
"het geselecteerde gebied en verbeterd volgen van de niet-rechthoekige "
"gebieden of objecten. Het gebruikt slechts 2 standaard mogelijkheden (HoGs "
"en Kleurnamen). Het werkt ook op een vergelijkbare lagere fps (25 fps) maar "
"geeft hogere nauwkeurigheid voor volgen van objecten."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:62
msgid "MOSSE"
msgstr "MOSSE"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:64
msgid "**Minimum Output Sum of Squared Error**"
msgstr "**Minimum uitvoersom van kwadraat van fout**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:66
msgid ""
"MOSSE uses an adaptive correlation for object tracking which produces stable "
"correlation filters when initialized using a single frame. MOSSE tracker is "
"robust to variations in lighting, scale, pose, and non-rigid deformations. "
"It also detects occlusion based upon the peak-to-sidelobe ratio, which "
"enables the tracker to pause and resume where it left off when the object "
"reappears. MOSSE tracker also operates at a higher fps (450 fps and even "
"more)."
msgstr ""
"MOSSE gebruikt een adaptieve correlatie voor volgen van een object die "
"stabiele correlatiefilters produceert bij initialisatie met een enkel frame. "
"MOSSE volger is robuust bij variaties in verlichting, schaal, pose en niet-"
"rigide vervormingen. Het detecteert ook occlusie gebaseerd op de verhouding "
"piek-tot-zijlob, die de volger laat pauzeren en hervatten waar het stopte "
"toen het object weer verscheen. MOSSE volger werkt ook op een hogere fps "
"(450 fps en zelfs meer)."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:68
msgid "**Pros:** It is as accurate as other complex trackers and much faster."
msgstr ""
"**Voordeel:** Het is net zo nauwkeurig als andere complexe volgers en veel "
"sneller."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:70
msgid ""
"**Cons:** On a performance scale, it lags behind the deep learning based "
"trackers."
msgstr ""
"**Cons:** Op een prestatieschaal, het blijft achter op de \"deep learning\" "
"gebaseerde volgers."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:73
msgid "MIL"
msgstr "MIL"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:75
msgid ""
"**Pros:** The performance is pretty good. It does a reasonable job under "
"partial occlusion."
msgstr ""
"**Pros:** De prestatie is tamelijk goed. Het doet een redelijke job onder "
"gedeeltelijke occlusie."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:77
msgid ""
"**Cons:** Tracking failure is not reported reliably. Does not recover from "
"full occlusion."
msgstr ""
"**Cons:** Mislukken van volgen wordt niet betrouwbaar gerapporteerd. "
"Herstelt niet vanuit volledige occlusie."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:80
msgid "MedianFlow"
msgstr "MedianFlow"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:82
msgid ""
"**Pros:** Excellent tracking failure reporting. Works very well when the "
"motion is predictable and there is no occlusion."
msgstr ""
"**Pros:** Uitstekend rapporteren van mislukking van volgen. Werkt zeer goed "
"wanneer de beweging voorspelbaar is en er is geen occlusie."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:84
msgid "**Cons:** Fails under large motion."
msgstr "**Cons:** Mislukt onder grote beweging."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:87
msgid "`DaSiam <https://arxiv.org/pdf/1808.06048.pdf>`_"
msgstr "`DaSiam <https://arxiv.org/pdf/1808.06048.pdf>`_"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:89
msgid ""
"The DaSiamRPN visual tracking algorithm relies on deep-learning models to "
"provide extremely accurate results."
msgstr ""
"Het visuele volgalgoritme van DaSiamRPN vertrouwt op modellen uit deep-"
"learning modellen om extreem accurate resultaten te leveren."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:91
msgid ""
"In order to use the DaSiam algorithm you need to download the AI models and "
"place them in:"
msgstr ""
"Om het DaSiam-algoritme te gebruiken moet u de AI-modellen downloaden en ze "
"plaatsen in:"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:93
msgid "**Linux**"
msgstr "**Linux**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:95
msgid "*$HOME/.local/share/kdenlive/opencvmodels*."
msgstr "*$HOME/.local/share/kdenlive/opencvmodels*."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:97
msgid "Flatpak"
msgstr "Flatpak"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:99
msgid "*$HOME/.var/app/org.kde.kdenlive/data/kdenlive/opencvmodels*"
msgstr "*$HOME/.var/app/org.kde.kdenlive/data/kdenlive/opencvmodels*"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:101
msgid "**Windows**"
msgstr "**Windows**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:103
msgid "*%AppData%/kdenlive/opencvmodels*"
msgstr "*%AppData%/kdenlive/opencvmodels*"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:105
msgid ""
"Press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key simultaneously) "
"and copy **%AppData%/kdenlive/**. Then create the folder `opencvmodels`"
msgstr ""
"Druk op :kbd:`Win + R` (tegelijk op toets :kbd:`Windows` en :kbd:`R`) en **"
"%AppData%/kdenlive/** kopiëren. Maak daarna de map `opencvmodels` aan"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:107
msgid ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_cls1.onnx"
msgstr ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_cls1.onnx"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:108
msgid ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_kernel_r1."
"onnx"
msgstr ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_kernel_r1."
"onnx"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:109
msgid ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_model.onnx"
msgstr ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_model.onnx"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:112
msgid "Frame shape"
msgstr "Framevorm"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:114
#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:119
msgid "soon"
msgstr "spoedig"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:117
msgid "Shape color"
msgstr "Kleur van vorm"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:122
msgid "Blur type"
msgstr "Type vervagen"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:127
msgid ""
"Four blur types are available: Median blur, Gaussian blur, Pixelate, Opaque "
"fill"
msgstr ""
"Er zijn vier verdoezeltypen beschikbaar: Middel vervaging, Gaussische "
"vervaging, Pixels aanbrengen, Dekkend vullen"
