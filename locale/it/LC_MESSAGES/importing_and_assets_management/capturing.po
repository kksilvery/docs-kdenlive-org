# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-30 00:37+0000\n"
"PO-Revision-Date: 2022-05-01 15:55+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.0\n"

#: ../../importing_and_assets_management/capturing.rst:19
msgid "Capturing Video"
msgstr "Cattura video"

#: ../../importing_and_assets_management/capturing.rst:22
msgid "Contents"
msgstr "Contenuto"

#: ../../importing_and_assets_management/capturing.rst:26
msgid ""
"At least Firewire and webcam capture were removed in porting to KDE 5 due to "
"lack of manpower."
msgstr ""
"Come minimo la cattura da Firewire o da webcam sono state rimosse nella "
"conversione a KDE 5 per mancanza di personale."

#: ../../importing_and_assets_management/capturing.rst:29
msgid ""
"**Kdenlive** provides functionality for capturing video from external "
"devices; e.g., Firewire, FFmpeg, Screen Grab and Blackmagic."
msgstr ""
"**Kdenlive** fornisce delle funzionalità di cattura video da dispositivi "
"esterni, quali Firewire, FFmpeg, cattura dello schermo e Blackmagic."

#: ../../importing_and_assets_management/capturing.rst:32
msgid ""
"You configure video capturing from :menuselection:`Settings --> Configure "
"Kdenlive --> Capture` (more on this :ref:`configure_kdenlive`)."
msgstr ""
"Puoi configurare la cattura video da :menuselection:`Impostazioni --> "
"Configura Kdenlive --> Cattura` (per altre informazioni su questo argomento, "
"vedi :ref:`configure_kdenlive`)."

#: ../../importing_and_assets_management/capturing.rst:35
msgid ""
"You define the destination location for your captures by using :"
"menuselection:`Settings --> Configure Kdenlive --> Environment --> Default "
"Folders` (more on this :ref:`configure_kdenlive`)."
msgstr ""
"Definisci la posizione di destinazione per le catture usando :menuselection:"
"`Impostazioni --> Configura Kdenlive --> Ambiente --> Cartelle predefinite` "
"(per altre informazioni su questo argomento, vedi :ref:`configure_kdenlive`)."

#: ../../importing_and_assets_management/capturing.rst:38
msgid ""
"To execute a video capture, select the :ref:`monitors` and choose the "
"capture device from the dropdown in the bottom right."
msgstr ""
"Per eseguire una cattura video, seleziona i :ref:`monitors` e scegli il "
"dispositivo di cattura dal menu a cascata in basso a destra."

#: ../../importing_and_assets_management/capturing.rst:47
msgid "Firewire"
msgstr "Firewire"

#: ../../importing_and_assets_management/capturing.rst:51
msgid ""
"This option is not available in recent versions of Kdenlive. Use dvgrab "
"directly in a terminal to capture video from firewire."
msgstr ""
"Questa opzione non è disponibile nelle ultime versioni di Kdenlive. Usa "
"direttamente dvgrab in un terminale per catturare il video da firewire."

#: ../../importing_and_assets_management/capturing.rst:54
msgid ""
"This captures video from sources connected via a firewire (also known as -  "
"IEEE 1394 High Speed Serial Bus) card and cable. This functionality uses the "
"`dvgrab <http://linux.die.net/man/1/dvgrab>`_ program and the settings for "
"this can be customized by clicking the spanner icon or choosing  :"
"menuselection:`Settings>Configure Kdenlive`.  See :ref:`configure_kdenlive`."
msgstr ""
"Cattura il video da  sorgenti connesse tramite una scheda firewire "
"(conosciuta anche come IEEE 1394 High Speed Serial Bus) e cavo. Questa "
"funzionalità usa il programma `dvgrab <http://linux.die.net/man/1/dvgrab>`_: "
"le sue impostazioni possono essere personalizzate facendo clic sull'icona a "
"chiave inglese, oppure scegliendo :menuselection:`Impostazioni>Configura "
"Kdenlive`.  Vedi :ref:`configure_kdenlive`."

#: ../../importing_and_assets_management/capturing.rst:57
msgid "To perform a capture:"
msgstr "Per eseguire una cattura:"

#: ../../importing_and_assets_management/capturing.rst:60
msgid "Plug in your device to the firewire card and turn it on to play mode"
msgstr ""
"Collega il dispositivo alla scheda firewire, e mettilo nella modalità di "
"riproduzione"

#: ../../importing_and_assets_management/capturing.rst:63
msgid "Click the *Connect Button*"
msgstr "Fai clic sul *pulsante Connetti*"

#: ../../importing_and_assets_management/capturing.rst:69
msgid ""
"Click the Record Button – note it toggles to grey while you are recording"
msgstr ""
"Fai clic sul pulsante Registra - nota che diventa grigio durante la "
"registrazione"

#: ../../importing_and_assets_management/capturing.rst:72
msgid ""
"Click the Record button again to stop capture. Or click the stop button."
msgstr ""
"Fai nuovamente clic sul pulsante Registra per fermare la cattura, oppure fai "
"clic sul pulsante di interruzione."

#: ../../importing_and_assets_management/capturing.rst:75
msgid "Once capturing is finished, click the disconnect button"
msgstr "Una volta terminata la cattura fai clic sul pulsante di disconnessione"

#: ../../importing_and_assets_management/capturing.rst:82
msgid ""
"In the *Captured Files* dialog, click the import button to have the captured "
"files automatically imported into the project bin."
msgstr ""
"Fai clic sul pulsante di importazione nella finestra *File catturati* per "
"importare automaticamente i file catturati nel contenitore del progetto."

#: ../../importing_and_assets_management/capturing.rst:92
msgid ""
"If your device does not start playing the source device when you click the "
"record button, you may have to start playback on your device manually and "
"then click record."
msgstr ""
"Se il dispositivo non inizia a riprodurre il dispositivo sorgente quando fai "
"clic sul pulsante di registrazione, allora potresti dover avviare "
"manualmente la riproduzione e poi fare clic su registra."

#: ../../importing_and_assets_management/capturing.rst:96
msgid "FFmpeg"
msgstr "FFmpeg"

#: ../../importing_and_assets_management/capturing.rst:98
msgid ""
"I believe this captures video from an installed Web Cam using *Video4Linux2*."
msgstr ""
"Credo che catturi il video da una webcam installata usando *Video4Linux2*."

#: ../../importing_and_assets_management/capturing.rst:103
msgid "Screen Grab"
msgstr "Cattura dello schermo"

#: ../../importing_and_assets_management/capturing.rst:105
msgid "This captures video of the PC screen."
msgstr "Cattura il video dello schermo del PC."

#: ../../importing_and_assets_management/capturing.rst:107
msgid "Open screen grab: :menuselection:`View --> Screen Grab`."
msgstr ""
"Apri la cattura dello schermo: :menuselection:`Visualizza --> Cattura dello "
"schermo`."

#: ../../importing_and_assets_management/capturing.rst:109
msgid "Start recording: click the “record” button."
msgstr "Inizia la registrazione: fai clic sul pulsante “registra”."

#: ../../importing_and_assets_management/capturing.rst:112
msgid "Stop record: click the \"record\" button again."
msgstr ""
"Interrompi la registrazione: fai nuovamente clic sul pulsante “registra”."

#: ../../importing_and_assets_management/capturing.rst:115
msgid "The recorded clip will be added in the project bin."
msgstr "La clip registrata verrà aggiunta al contenitore del progetto."

#: ../../importing_and_assets_management/capturing.rst:118
msgid "Settings can be adjusted in :ref:`configure_kdenlive`"
msgstr "Le impostazioni possono essere regolate in :ref:`configure_kdenlive`"

#: ../../importing_and_assets_management/capturing.rst:121
msgid ""
"To check on your linux distro, type ``ffmpeg -version`` in a terminal and "
"look for ``--enable-x11grab`` in the reported configuration info.  [1]_"
msgstr ""
"Per verificare la distribuzione Linux, digita ``ffmpeg -version`` in un "
"terminale e cerca ``--enable-x11grab`` nelle informazioni sulla "
"configurazione riportate.  [1]_"

#: ../../importing_and_assets_management/capturing.rst:124
msgid ""
"If you are capturing the screen and using the X246 with audio settings and "
"you get a crash as shown in the screen shot…"
msgstr ""
"Se stai catturando lo schermo usando X246 con le impostazioni audio e si "
"verifica un arresto anomalo, come nella schermata..."

#: ../../importing_and_assets_management/capturing.rst:130
msgid ""
"…then consider creating a profile for audio capture where ``-acodec "
"pcm_s16le``  is replaced by ``-acodec libvorbis -b 320k``. See :ref:"
"`configure_kdenlive`."
msgstr ""
"...considera la creazione di un profilo per la cattura audio, nel quale ``-"
"acodec pcm_s16le`` sia sostituito da ``-acodec libvorbis -b 320k``. Vedi :"
"ref:`configure_kdenlive`."

#: ../../importing_and_assets_management/capturing.rst:134
msgid "Blackmagic"
msgstr "Blackmagic"

#: ../../importing_and_assets_management/capturing.rst:138
msgid ""
"This is for capturing from Blackmagics `decklink <http://www.blackmagic-"
"design.com/uk/products/decklink/>`_ video capture cards (AFAIK). Not sure "
"how stable this code is at the moment. Mentioned in legacy Mantis bug "
"tracker ID 2130."
msgstr ""
"Per quel che ne so serve per la cattura con le schede video `decklink "
"<http://www.blackmagic-design.com/uk/products/decklink/>`_ di Blackmagics. "
"Non sono sicuro di quanto sia stabile il codice al momento. Viene menzionato "
"nel bug numero 2130 del vecchio sistema di tracciamento dei bug."

#: ../../importing_and_assets_management/capturing.rst:142
msgid "Footnotes"
msgstr "Note"

#: ../../importing_and_assets_management/capturing.rst:146
msgid ""
"There are now two branches of *ffmpeg*: a *Libav* branch and an ffmpeg.org "
"branch. The *ffmpeg* version from the latter branch reports the "
"configuration when you run with ``ffmpeg -version``. The *Libav* version "
"does not. So this method to check for the ``--enable-x11grab`` does not work "
"if you have the *Libav* version of *ffmpeg*."
msgstr ""
"Adesso ci sono due rami di *ffmpeg*: uno *Libav* e uno di ffmpeg.org. La "
"versione *ffmpeg* dell'ultimo ramo riporta la configurazione quando esegui "
"``ffmpeg -version``, mentre quella di *Libav* non lo fa. Quindi il metodo di "
"controllare ``--enable-x11grab`` non funziona se hai la versione *Libav* di "
"*ffmpeg*."
