# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-28 21:11+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/volume_keyframable.rst:11
msgid "Volume (keyframable)"
msgstr "Volume (keyframable)"

#: ../../effects_and_compositions/effect_groups/audio_correction/volume_keyframable.rst:13
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/audio_correction/volume_keyframable.rst:15
msgid ""
"This is an audio effect to change the volume of a clip using keyframes "
"(change of effect over time.) Volume (Keyframable) uses decibels as opposed "
"to `Gain <https://userbase.kde.org/Kdenlive/Manual/Effects/Audio_Correction/"
"Gain>`_."
msgstr ""
"Dit is een audio-effect om het volume van een clip te wijzigen met keyframes "
"(wijziging van het effect gedurende de tijd.) Volume (Keyframable) gebruikt "
"decibels in tegenstelling tot `Versterking <https://userbase.kde.org/"
"Kdenlive/Manual/Effects/Audio_Correction/Gain>`_."
