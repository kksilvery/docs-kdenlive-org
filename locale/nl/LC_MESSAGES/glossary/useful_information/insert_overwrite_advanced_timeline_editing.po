# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-19 00:37+0000\n"
"PO-Revision-Date: 2022-02-17 12:19+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:13
msgid "Insert and Overwrite: advanced timeline editing"
msgstr "Invoegen en overschrijven: geavanceerde tijdlijn bewerking"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:17
msgid ""
"Kdenlive offers advanced timeline editing functions. In this article we’re "
"looking at the :menuselection:`insert` |timeline-insert| and :menuselection:"
"`overwrite` |timeline-overwrite| advanced editing functions. A later article "
"then will cover the opposite :menuselection:`lift` |timeline-lift| and :"
"menuselection:`extract` |timeline-extract| functions."
msgstr ""
"Kdenlive biedt geavanceerde bewerkingsfuncties van de tijdlijn. In dit "
"artikel kijken we naar de geavanceerde bewerkingsfuncties :menuselection:"
"`invoegen` |timeline-insert| en :menuselection:`overschrijven` |timeline-"
"overwrite|. In een later artikel zullen we de tegenovergestelde functies :"
"menuselection:`liften` |timeline-lift| en :menuselection:`extraheren` |"
"timeline-extract| dekken."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:19
msgid ""
"When inserting or overwriting some part in the timeline with some part from "
"a clip, we now face two zones, so how does this work out at all? We only "
"want to deal with three points, that is, with one zone and a point (for that "
"reason this is also sometimes termed three point editing). In consequence, "
"there are two different insert/overwrite operations:"
msgstr ""
"Bij invoegen of overschrijven van een gedeelte in de tijdlijn door een "
"gedeelte van een clip, hebben we te maken met twee zones, dus hoe werkt dat "
"helemaal? We willen slechts te maken hebben met drie punten, dat wil zeggen, "
"met één zone en een punt (om die reden wordt ook wel de term "
"driepuntsbewerking gebruikt). Als gevolg hiervan zijn er twee verschillende "
"invoeg-/overschrijfbewerkingen:"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:21
msgid ""
"insert/overwrite a clip zone into timeline at some point (cursor/playhead), "
"or"
msgstr ""
"voeg in/overschrijf een clipzone in de tijdlijn op een punt (cursor/"
"afspeellijn), or"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:22
msgid "insert/overwrite a clip starting at some point into a timeline zone."
msgstr ""
"voeg in/overschrijf een clip beginnend op een punt in een tijdlijnzone."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:25
msgid "Insert Clip Zone into Timeline at Timeline Cursor"
msgstr "Clipzone invoegen in de tijdlijn op de tijdlijncursor"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:35
msgid ""
"As we’re going to insert a clip zone into the timeline, first make sure that "
"the :menuselection:`switch for using timeline zone is crossed out` |timeline-"
"use-zone-off| (it’s *off*). This is also shown in the screenshot. (You’ll "
"find this switch above the track headers, next to the track size zoom in/out "
"controls.)"
msgstr ""
"Omdat we een clipzone in de tijdlijn gaan invoegen, zorgen we eerst dat de :"
"menuselection:`schakelaar voor gebruik van tijdlijnzone is inactief` |"
"timeline-use-zone-off| (het is *uit*). Dit wordt ook getoond in de "
"schermafdruk. (Deze schakelaar is te vinden boven de trackkoppen, naast de "
"besturing van de trackgrootte in/uitzoomen.)"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:37
msgid ""
"A visual clue (albeit a rather unintrusive one) is that the **timeline zone "
"bar** is now *dimmed*."
msgstr ""
"Een visuele clue (hoewel een tamelijk onbelangrijke) is dat de "
"**tijdlijnzonebalk** nu is *gedimd*."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:46
msgid ""
"Next, mark the **clip region** of the source clip you want to insert into "
"the timeline. You do this as usual, using either the :kbd:`I` and :kbd:`O` "
"shortcuts, or the set zone in/out buttons of the clip monitor."
msgstr ""
"Markeer, vervolgens, het **clipgebied** van de bronclip die u wilt invoegen "
"in de tijdlijn. Dit doet u zoals gebruikelijk, met ofwel de sneltoetsen :kbd:"
"`I` en :kbd:`O` of de knoppen zone in/uit zetten van de clipmonitor."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:55
msgid ""
"Now **place the timeline cursor** to where you want to start with the insert."
msgstr "**Plaats de tijdlijncursor** nu op waar u het invoegen wilt starten."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:57
msgid ""
"Also make sure to **select the correct track** for insertion – using the :"
"kbd:`cursor up` and :kbd:`cursor down` keys. (Remember that the currently "
"selected track is marked with the semi-transparent selection color, the "
"exact color of which depends on your particular color theme.)"
msgstr ""
"Controleer ook **selecteer de juiste track** voor invoegen – met de toetsen :"
"kbd:`cursor omhoog` en :kbd:`cursor omlaag`. (Bedenk dat de nu geselecteerde "
"track gemarkeerd is met de half-transparante selectiekleur, de exacte kleur "
"hangt af van uw specifieke kleurenthema.)"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:66
msgid ""
"Finally press the :kbd:`v` shortcut, or click the :menuselection:`insert "
"clip zone toolbar button` |timeline-insert|, or use :menuselection:`Timeline "
"--> Insertion --> Insert Clip Zone in Timeline`."
msgstr ""
"Druk tenslotte op de sneltoets :kbd:`v` of klik op :menuselection:"
"`werkbalkknop clipzone invoegen` |timeline-insert| of gebruik :menuselection:"
"`Tijdlijn --> Invoegen --> Clipzone invoegen in tijdlijn`."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:72
msgid ""
"Insertion starts from the timeline cursor, and not from the timeline zone "
"start (because we chose to ignore it in our very first step)."
msgstr ""
"Invoegen begint vanaf de tijdlijncursor en niet vanaf het begin van de "
"tijdlijnzone (omdat we in onze allereerste stap gekozen hebben dat te "
"negeren)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:73
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:124
msgid "Locked tracks are unaffected, such as the topmost track in our example."
msgstr ""
"Vergrendelde tracks zijn onaangeroerd, zoals de bovenste track in ons "
"voorbeeld."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:74
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:125
msgid ""
"Unlocked tracks get affected in that whatever is at the insertion point and "
"later in the timeline gets shifted away to make room for the insertion."
msgstr ""
"Niet vergrendelde tracks worden aangeroerd op datgene dat op het invoegpunt "
"en later in de tijdlijn verschoven wordt om ruimte te maken voor de "
"invoeging."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:77
msgid "Insert Clip (from In Point) into Timeline Zone"
msgstr "Clip invoegen (vanaf punt In) in de tijdlijnzone"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:86
msgid ""
"This time, we’re going to insert some part of a clip to *exactly fit* into "
"the timeline zone. So we now need to switch on using the :menuselection:"
"`timeline zone` |timeline-use-zone-on|. This is also shown in the "
"screenshot. (You’ll find this switch above the track headers, next to the "
"track size zoom in/out controls.)"
msgstr ""
"Deze keer gaan we een gedeelte van een clip invoegen om *exact te passen* in "
"de tijdlijnzone. Dus moeten we nu omschakelen naar gebruik van de :"
"menuselection:`tijdlijnzone` |timeline-use-zone-on|. Dit wordt ook getoond "
"in de schermafdruk. (U vindt deze schakelaar boven de trackkoppen, naast de "
"besturing van de trackgrootte in/uitzoomen.)"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:88
msgid ""
"A visual clue (albeit a rather unintrusive one) is that the **timeline zone "
"bar** is now *bright*."
msgstr ""
"Een visuele clue (hoewel een tamelijk onbelangrijke) is dat de "
"**tijdlijnzonebalk** nu *helder* is."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:97
msgid ""
"This time, we only need to **set the in point** for our source clip. The out "
"point doesn’t matter, as it will be later determined automatically by the "
"length of the timeline zone."
msgstr ""
"Deze keer moeten we alleen **stel het punt In in** voor onze bronclip. Het "
"punt Uit doet niet ter zake, omdat het later automatisch bepaalt zal worden "
"door de lengte van de tijdlijnzone."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:106
msgid ""
"Now, place **mark the timeline zone** into which you want to insert a part "
"of your source clip. Notice that the timeline cursor position now doesn’t "
"matter."
msgstr ""
"Plaats nu **de tijdlijnzone markeren** daar waar u een gedeelte van uw "
"bronclip wilt invoegen. Merk op dat de tijdlijncursorpositie nu niet ter "
"zake doet."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:108
msgid ""
"Make sure to **select the correct track** for insertion – using the :kbd:"
"`cursor up` and :kbd:`cursor down` keys."
msgstr ""
"Controleer ook **selecteer de juiste track** voor invoegen – met de toetsen :"
"kbd:`cursor omhoog` en :kbd:`cursor omlaag`."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:117
msgid ""
"Finally press the :kbd:`V` shortcut, or click the :menuselection:`insert "
"clip zone toolbar button` |timeline-insert|, or use :menuselection:`Timeline "
"--> Insertion --> Insert Clip Zone in Timeline`."
msgstr ""
"Druk tenslotte op de sneltoets :kbd:`V` of klik op :menuselection:"
"`werkbalkknop clipzone invoegen` |timeline-insert| of gebruik :menuselection:"
"`Tijdlijn --> Invoegen --> Clipzone invoegen in tijdlijn`."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:123
msgid ""
"Insertion starts from the beginning of the timeline zone, and not from the "
"timeline cursor position (because we chose to enable the timeline zone in "
"our very first step)."
msgstr ""
"Invoegen begint vanaf de het begin van de tijdlijnzone en niet vanaf de "
"tijdlijncursor (omdat we in onze allereerste stap gekozen hebben de "
"tijdlijnzone in te schakelen)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:129
msgid "Overwrite Timeline with Clip Zone"
msgstr "Tijdlijn overschrijven met clipzone"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:131
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:138
msgid ":menuselection:`overwrite` |timeline-overwrite|"
msgstr ":menuselection:`overschrijven` |timeline-overwrite|"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:133
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:140
msgid "(will be documented later)"
msgstr "(zal later gedocumenteerd worden)"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:136
msgid "Overwrite Timeline Zone with Clip"
msgstr "Tijdlijnzone overschrijven met clip"
