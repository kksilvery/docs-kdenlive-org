# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-28 11:41+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/clip_menu/extract_audio.rst:16
msgid "Extract Audio"
msgstr "Видобути звук"

#: ../../user_interface/menu/clip_menu/extract_audio.rst:18
msgid "Contents"
msgstr "Зміст"

#: ../../user_interface/menu/clip_menu/extract_audio.rst:20
msgid ""
"This feature extracts the audio out of a video clip into a :file:`.WAV` file "
"and adds it to the :ref:`project_tree`."
msgstr ""
"За допомогою цього пункту можна видобути звукові дані з відеокліпу до файла :"
"file:`.WAV` і додати його до :ref:`контейнера проєкту <project_tree>`."

#: ../../user_interface/menu/clip_menu/extract_audio.rst:22
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin or under the :ref:`clip_menu` when a clip is selected in the "
"Project Bin."
msgstr ""
"Цим пунктом меню можна скористатися за допомогою контекстного меню пункту "
"кліпу на :ref:`панелі контейнера проєкту <project_tree>` або за допомогою :"
"ref:`меню «Кліп» <clip_menu>`, якщо кліп позначено на панелі контейнера "
"проєкту."

#: ../../user_interface/menu/clip_menu/extract_audio.rst:28
msgid "The process runs as a job in the Project Bin."
msgstr "Процес запускає завдання на панелі контейнера проєкту."
