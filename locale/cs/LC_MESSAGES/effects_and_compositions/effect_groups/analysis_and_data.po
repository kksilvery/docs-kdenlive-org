# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vpelcak@suse.cz>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-18 09:22+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:12
msgid "Analysis and Data"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:14
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:16
msgid ""
"The Analysis and Data category of effects are useful for getting information "
"about your video material rather than manipulating the imagery. The "
"exception is Timeout Indicator - it can be used to add an effect to your "
"video."
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:18
msgid "Effects in this category"
msgstr "Efekty v této kategorii"

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:25
msgid ":ref:`audio_wave`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:26
msgid ":ref:`rgb_parade`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:27
msgid ":ref:`timeout_indicator`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data.rst:28
msgid ":ref:`vectorscope`"
msgstr ""
