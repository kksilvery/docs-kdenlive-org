# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 17:46+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: MLT clip\n"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:14
msgid "Dust"
msgstr "Pó"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:16
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:18
msgid ""
"See `dust filter <https://www.mltframework.org/plugins/FilterDust/>`_ from "
"MLT."
msgstr ""
"Veja o `filtro de pó <https://www.mltframework.org/plugins/FilterDust/>`_ do "
"MLT."

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:20
msgid "Add dust and specks to the video clip, as in old movies."
msgstr "Adiciona pó e riscos ao 'clip' de vídeo, como nos filmes antigos."

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:22
msgid "https://youtu.be/h0s0PBfpcEE"
msgstr "https://youtu.be/h0s0PBfpcEE"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:24
msgid "https://youtu.be/wbX7Df8rC0M"
msgstr "https://youtu.be/wbX7Df8rC0M"
