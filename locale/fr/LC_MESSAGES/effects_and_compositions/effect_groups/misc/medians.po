# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-31 14:31+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:12
msgid "Medians"
msgstr "Médians"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:14
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:16
msgid "This filter implements several median type filters."
msgstr "Ce filtre intègre plusieurs filtres de type médian."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:18
msgid "INTRODUCTION:"
msgstr "INTRODUCTION :"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:20
msgid ""
"Median is a quite popular non-linear filter in image processing. Most often "
"it is used to remove impulsive noise, like \"salt &  pepper\" noise, \"dead"
"\" and \"hot\" pixels, dirt on film, etc. This is because it behaves kind of "
"\"inversely\" compared to linear denoisers. The more a bad pixel stands out "
"from the surrounding area, the bigger residual it will leave with a linear "
"filter - but the more probably it will be eliminated by the median."
msgstr ""
"La médiane est un filtre non linéaire assez populaire dans le traitement "
"d'images. Il est le plus souvent utilisé pour supprimer les bruits en "
"impulsion, comme le bruit « poivre et sel », les pixels « morts » et "
"« chauds », la neige sur les films, etc. Ceci est dû au fait qu'il se "
"comporte en quelque sorte « inversement » par rapport aux suppresseurs "
"linéaires de bruit. Plus un mauvais pixel se détache de la zone "
"environnante, plus le résidu qu'il laissera avec un filtre linéaire sera "
"important - mais plus il sera probablement éliminé par la médiane."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:22
msgid ""
"The down side is that the median operation is quite slow. As an order-"
"statistic filter, it is similar to sorting, that must be done for each "
"pixel, so using a fast algorithm is very important. For the small medians, "
"the algorithms of the type described in `this page <http://ndevilla.free.fr/"
"median/median/src/optmed.c>`_ are used here, with some small modifications "
"for a further slight improvement in speed. For the \"Variable size\" median, "
"code from  [1]_  is used. The compound filters (ArceBI, ML3D, ML3dEX) are "
"made according to the formulas given in the corresponding work [2]_. For "
"more info on median filtering see `Wikipedia article <https://en.wikipedia."
"org/wiki/Median_filter>`_."
msgstr ""
"L'inconvénient est que l'opération médiane est assez lente. En tant que "
"filtre statistique ordonnée, elle s'apparente à un tri, devant être effectué "
"pour chaque pixel, donc l'utilisation d'un algorithme rapide est très "
"importante. Pour les petites médianes, les algorithmes du type décrit dans "
"« cette page <http://ndevilla.free.fr/median/median/src/optmed.c> »_ sont "
"utilisés ici, avec quelques petites modifications pour une légère "
"amélioration supplémentaire de la vitesse. Pour la médiane de « taille "
"variable », le code de [1]_ est utilisé. Les filtres composés (ArceBI, ML3D, "
"ML3dEX) sont réalisés selon les formules données dans l'ouvrage "
"correspondant [2]_. Pour plus d'informations sur le filtrage médian, "
"veuillez consulter l'article de « Wikipedia <https://fr.wikipedia.org/wiki/"
"Filtre_m%C3%A9dian> »_."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:24
msgid "IMPLEMENTED ALGORITHMS:"
msgstr "ALGORITHMES IMPLÉMENTÉS :"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:27
msgid "**Cross5**"
msgstr "**Croix 5**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:27
msgid "Median of the pixel with its top, bottom, left and right neighbor."
msgstr ""
"Médiane du pixel avec son voisin supérieur, inférieur, gauche et droit."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:30
msgid "**Square3x3**"
msgstr "**Carré 3 x 3**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:30
msgid "Median of the pixel with the surrounding 8 pixels. (3x3 box)"
msgstr ""
"Médiane du pixel avec la boîte environnante de 8 pixels (Boîte de 3 x 3)."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:33
msgid "**Bilevel**"
msgstr "**Bi-niveau**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:33
msgid ""
"First, make cross5 median, then make median of the pixel with its four "
"diagonal neighbors, and finally take the median of the pixel and the two "
"previously calculated medians. Slightly better preserves detail than the "
"simple medians above."
msgstr ""
"D'abord, faire la médiane « croisée 5 », puis la médiane du pixel avec ses "
"quatre voisins diagonaux et enfin prendre la médiane du pixel et des deux "
"médianes calculées précédemment. Ceci préserve un peu mieux les détails que "
"les médianes simples ci-dessus."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:36
msgid "**Diamond3x3**"
msgstr "**Diamant 3 x 3**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:36
msgid ""
"Takes median of the pixel with 12 neighboring pixels arranged in a diamond "
"pattern."
msgstr ""
"Prend la médiane du pixel avec 12 pixels voisins disposés selon un forme en "
"losange."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:39
msgid "**Square5x5**"
msgstr "**Carré 5 x 5**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:39
msgid "Median of the pixel with the 5x5 surrounding box."
msgstr "Médiane du pixel avec la boîte environnante de 5 x 5."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:42
msgid "**Temp3**"
msgstr "**Temp3**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:42
msgid ""
"Temporal only median of three frames. Can be used to reduce single frame "
"time-impulsive noise like photoflash. Delays the video by 1 frame."
msgstr ""
"Médiane temporelle uniquement sur trois trames. Peut être utilisé pour "
"réduire le bruit impulsif temporel d'une seule trame, comme le flash photo. "
"Retarde la vidéo d'une image."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:45
msgid "**Temp5**"
msgstr "**Temp5**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:45
msgid ""
"Temporal only median of five frames. Can be used to reduce double frame time-"
"impulsive noise, but the artifacts on fast moving objects are stronger than "
"with temp3. Delays the video by 2 frames."
msgstr ""
"Médiane temporelle seulement de cinq trames. Peut être utilisé pour réduire "
"le bruit en impulsion temporelle à deux trames. Cependant, les artefacts sur "
"les objets en mouvement rapide sont plus forts qu'avec « temp 3 ». Ceci "
"retarde la vidéo de 2 trames."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:48
msgid "**ArceBI**"
msgstr "**ArceBI**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:48
msgid ""
"Spatio-temporal multilevel median, as described by Arce. See the "
"corresponding work [2]_. Delays the video by 1 frame."
msgstr ""
"Médiane multi-niveaux spatio-temporelle, telle que décrite par Arce. "
"Veuillez consulter le travail correspondant [2]_. Retarde la vidéo d'une "
"trame."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:51
msgid "**ML3D**"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:51
msgid ""
"Spatio-temporal multilevel median, as described by Alp. See the "
"corresponding work [2]_. Delays the video by 1 frame."
msgstr ""
"Médiane multi-niveaux spatio-temporelle, telle que décrite par Alp. Veuillez "
"consulter le travail correspondant [2]_. Retarde la vidéo d'une trame."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:54
msgid "**ML3dEX**"
msgstr "**ML3dEX**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:54
msgid ""
"Spatio-temporal multilevel median. Further development of ML3D by Kokaram, "
"see the corresponding work [2]_. Delays the video by 1 frame."
msgstr ""
"Médiane multi-niveaux spatio-temporelle. Développement ultérieur de "
"« ML3D », par Kokaram. Veuillez consulter le travail correspondant [2]_. "
"Retarde la vidéo d'une trame."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:57
msgid "**VarSize**"
msgstr "**Taille-Var**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:57
msgid ""
"Simple spatial only median in a user selected size square box around each "
"pixel. The effect could be described as \"quasi edge preserving, corner "
"rounding, small stuff eliminator\". Or maybe just an \"artsy blur\"."
msgstr ""
"Médiane uniquement spatiale simple dans un espace carré selon une taille "
"sélectionnée par l'utilisateur autour de chaque pixel. L'effet pourrait être "
"décrit comme une « quasi-préservation des bords, un arrondissement des "
"coins, un suppresseur de petites choses », ou peut-être simplement un « flou "
"artistique »."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:59
msgid "PARAMETERS:"
msgstr "PARAMÈTRES :"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:62
msgid "**Type**"
msgstr "**Type**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:62
msgid "Selects one of the eleven algorithms."
msgstr "Sélectionne l'un des onze algorithmes."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:65
msgid ""
"Only active when \"VarSize\" type is selected. Determines the size of the "
"square area over which the median is taken."
msgstr ""
"Actif uniquement lorsque le type « VarSize » est sélectionné. Détermine la "
"taille de la zone carrée sur laquelle la médiane est prise."

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:66
msgid "**Size**"
msgstr "**Taille**"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:68
msgid ""
"Simon Perreault, Patrick Hebert: Median filtering in constant time, `PDF "
"version <https://nomis80.org/ctmf.pdf>`_, `HTML version <https://nomis80.org/"
"ctmf.html>`_"
msgstr ""
"Simon Perreault, Patrick Hebert : filtrage médian dans un temps constant, "
"« Version PDF <https://nomis80.org/ctmf.pdf> »_, « Version HTML <https://"
"nomis80.org/ctmf.html> »_"

#: ../../effects_and_compositions/effect_groups/misc/medians.rst:70
msgid ""
"`Anil Christopher Kokaram: Motion Picture Restoration, Ph.D. thesis <https://"
"citeseerx.ist.psu.edu/viewdoc/download?"
"doi=10.1.1.36.9618&rep=rep1&type=pdf>`_"
msgstr ""
"« Anil Christopher Kokaram : Picture Restoration, Ph.D. thesis <https://"
"citeseerx.ist.psu.edu/viewdoc/download?"
"doi=10.1.1.36.9618&rep=rep1&type=pdf> »_"
