# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
# Ronald Stroethoff <stroe43@zonnet.nl>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-07-24 10:54+0200\n"
"Last-Translator: Ronald Stroethoff <stroe43@zonnet.nl>\n"
"Language-Team: vertalen\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: ../../glossary/useful_information/surround_sound.rst:13
msgid "Editing Surround Sound with Kdenlive"
msgstr "Surround-Sound met Kdenlive bewerken"

#: ../../glossary/useful_information/surround_sound.rst:15
msgid "Contents"
msgstr "Inhoud"

#: ../../glossary/useful_information/surround_sound.rst:17
msgid ""
"At the time of writing, **Kdenlive** only supports rendering a project to a "
"video containing stereo audio. It is not possible to render to more audio "
"channels or to explicitly map audio tracks to channels in the rendered "
"audio. In order to edit and create surround sound, some manual steps, "
"including external tools, are required."
msgstr ""
"Op het moment van schrijven ondersteunt **Kdenlive** alleen het renderen van "
"een project naar een video met stereo-audio. Het is niet mogelijk om naar "
"meer audiokanalen te renderen of om expliciet audio-tracks aan kanalen te "
"verbinden in de gerenderde audio. Om surround-sound te bewerken en te maken "
"zijn enige handmatige stappen, inclusief externe programma's vereist."

#: ../../glossary/useful_information/surround_sound.rst:20
msgid "This guide is using a 6 channel 5.1 surround sound as example."
msgstr ""
"Deze handleiding gebruikt een 6-kanaals 5.1 surround-geluid als voorbeeld."

#: ../../glossary/useful_information/surround_sound.rst:24
msgid "External Tools Used Here"
msgstr "Hier gebruikte externe programma's"

#: ../../glossary/useful_information/surround_sound.rst:26
msgid ""
"`Audacity <http://audacity.sourceforge.net/>`_ - Free Audio Editor and "
"Recorder"
msgstr ""
"`Audacity <http://audacity.sourceforge.net/>`_ - vrije audiobewerker en "
"recorder"

#: ../../glossary/useful_information/surround_sound.rst:27
msgid "`avconv <http://libav.org/avconv.html>`_ - A Video and Audio Converter"
msgstr ""
"`avconv <http://libav.org/avconv.html>`_ - een video- en audio-convertor"

#: ../../glossary/useful_information/surround_sound.rst:32
msgid ""
"Kdenlive uses ffmpeg, while on (k)ubuntu, ffmpeg is deprecated and avconv is "
"used instead. So these (and possibly other) distributions already have "
"avconv installed."
msgstr ""
"Kdenlive gebruikt ffmpeg, terwijl op (k)ubuntu, ffmpeg is afgekeurd en "
"avconv in plaats daarvan wordt gebruikt. Dus deze (en mogelijk andere) "
"distributies hebben avconv al geïnstalleerd."

#: ../../glossary/useful_information/surround_sound.rst:36
msgid "Creating New Surround Sound"
msgstr "Een nieuw surround sound creëren"

#: ../../glossary/useful_information/surround_sound.rst:38
msgid ""
"This guide describes one possible workaround using **Audacity** to create "
"and render a 5.1 surround sound audio track that can be added to the video "
"rendered by **Kdenlive**."
msgstr ""
"Deze handleiding beschrijft een mogelijke workaround dat **Audacity** "
"gebruikt voor de creatie en render van een 5.1 surround geluid audio track "
"die kan worden toegevoegd aan de video die is gerenderd door **Kdenlive**."

#: ../../glossary/useful_information/surround_sound.rst:42
msgid ""
"More advanced features such as surround panning (i.e. let a sound move from "
"rear to front) are beyond the capabilities of Audacity - but it is possible "
"to create similar effects manually."
msgstr ""
"Meer geavanceerde functionaliteiten zoals surround panning (d.w.z. een "
"geluid van achter naar voor verplaatsen) vallen buiten de mogelijkheden van "
"Audacity - maar het is mogelijk om vergelijkbare effecten handmatig uit te "
"creëren."

#: ../../glossary/useful_information/surround_sound.rst:46
msgid "Create and Edit Surround Sound with Audacity"
msgstr "Het creëren en bewerken van Surround Sound met Audacity"

#: ../../glossary/useful_information/surround_sound.rst:48
msgid ""
"The following example of a simple 5.1 surround sound is used in this guide:"
msgstr ""
"De volgende voorbeeld van een eenvoudig 5.1 surround sound is gebruikt in "
"deze handleiding:"

#: ../../glossary/useful_information/surround_sound.rst:51
msgid "Some original field recording from the front (stereo)"
msgstr "een originele veldopname van voren (stereo)"

#: ../../glossary/useful_information/surround_sound.rst:53
msgid "Some voice from the (front) center (mono)"
msgstr "Een stem van voren (front) midden (mono)"

#: ../../glossary/useful_information/surround_sound.rst:55
msgid "Some music from the rear (stereo)"
msgstr "Wat muziek van achteren (stereo)"

#: ../../glossary/useful_information/surround_sound.rst:58
msgid ""
"If, like in this example, some original field recording from a video clip is "
"supposed to be used to create the surround sound audio track, it can be "
"easily extracted using Kdenlive with :menuselection:`Extract Audio --> Wav "
"48000Hz` from the context menu of the clip. This creates a WAV audio file in "
"the same folder where the video clip is located."
msgstr ""
"Als, zoals in dit voorbeeld, een originele veldopname van een video clip "
"wordt verondersteld gebruikt te worden voor de creatie van een surround "
"sound audio track, dan kan het makkelijk met Kdenlive geëxtraheerd worden "
"met :menuselection:`Extract Audio --> Wav in het contextmenu van de clip. "
"Dit creëert een WAV audiobestand in dezelfde map als waar de video clip is "
"te vinden."

#: ../../glossary/useful_information/surround_sound.rst:60
msgid "The audio clips to be used in this example are:"
msgstr "De audio clips die in dit voorbeeld worden gebruikt zijn:"

#: ../../glossary/useful_information/surround_sound.rst:63
msgid ":file:`Field.wav` (stereo) for Front L+R"
msgstr ":file:`Field.wav` (stereo) voor Vooraan L+R"

#: ../../glossary/useful_information/surround_sound.rst:65
msgid ":file:`Voice.wav` (mono) for Center"
msgstr ":file:`Voice.wav` (mono) voor MIdden"

#: ../../glossary/useful_information/surround_sound.rst:67
msgid ":file:`Music.mp3` (stereo) for Surround L+R (rear)"
msgstr ":file:`Music.mp3` (stereo) voor Surround L+R (achter)"

#: ../../glossary/useful_information/surround_sound.rst:70
msgid ""
"In a new Audacity project, they can be imported in the above order with :"
"menuselection:`File --> Import --> Audio...`, the project should now look "
"something like this:"
msgstr ""
"In een nieuw Audacity project, kunt u ze importeren in bovenstaande volgorde "
"met :menuselection:`Bestand --> Importeren --> Audio...`, het project zou nu "
"er als volgt uit moeten zien:"

#: ../../glossary/useful_information/surround_sound.rst:76
msgid "The channel mapping for 5.1 surround sound is:"
msgstr "Het kanaal-gebruik zal voor 5.1 surround sound als volgt moeten zijn:"

#: ../../glossary/useful_information/surround_sound.rst:79
#: ../../glossary/useful_information/surround_sound.rst:198
msgid "1 - Front Left"
msgstr "1 - vooraan links"

#: ../../glossary/useful_information/surround_sound.rst:82
#: ../../glossary/useful_information/surround_sound.rst:200
msgid "2 - Front Right"
msgstr "2 - vooraan rechts"

#: ../../glossary/useful_information/surround_sound.rst:85
#: ../../glossary/useful_information/surround_sound.rst:202
msgid "3 - Center"
msgstr "3 - Midden"

#: ../../glossary/useful_information/surround_sound.rst:88
#: ../../glossary/useful_information/surround_sound.rst:204
msgid "4 - LFE"
msgstr "4 - LFE"

#: ../../glossary/useful_information/surround_sound.rst:91
#: ../../glossary/useful_information/surround_sound.rst:206
msgid "5 - Surround Left"
msgstr "5 - Surround links"

#: ../../glossary/useful_information/surround_sound.rst:94
#: ../../glossary/useful_information/surround_sound.rst:208
msgid "6 - Surround Right"
msgstr "6 - Surround rechts"

#: ../../glossary/useful_information/surround_sound.rst:99
#: ../../glossary/useful_information/surround_sound.rst:212
msgid ""
"LFE (Low Frequency Effects) is often referred to as \"subwoofer channel\", "
"which is not quite correct. A surround sound speaker setup is perfectly "
"valid without subwoofer. In this case the surround sound system will "
"redirect the LFE channel to \"large\" speakers, usually the front speakers."
msgstr ""
"Aan LFE (Low Frequency Effects) wordt vaak gerefereerd als het \"subwoofer "
"kanaal\", wat niet helemaal correct is. Een surround sound speaker setup is "
"perfect geldig zonder subwoofer. In dat geval zal het surround sound system "
"het LFE kanaal naar de \"grote\" speakers sturen, meestal de speakers "
"vooraan."

#: ../../glossary/useful_information/surround_sound.rst:102
msgid ""
"The stereo track \"Field\" can now be mapped to Front L+R, \"Voice\" to "
"Center and \"Music\" to Surround L+R. There is just one problem: the "
"Surround (rear) speakers of a surround speaker system are usually \"small\" "
"and not able to reproduce low frequencies. So it would be necessary to map "
"the low frequency range of the \"Music\" track to the LFE channel, otherwise "
"the music might sound a little \"thin\"."
msgstr ""
"De stereo track \"Field\" kan nu gekoppeld worden aan Vooraan L+R, \"Voice\" "
"aan Midden en \"Music\" aan Surround L+R. Er is alleen een probleem: de "
"Surround (achter) speakers van een surround speaker systeem zijn meestal "
"\"klein\" en niet in staat om lage frequenties te produceren. Daarom is het "
"dus noodzakelijk om de lage frequentie-range van de \"Music\" track te "
"koppelen aan het LFE kanaal, anders kan de muziek een beetje te \"dun\" "
"klinken."

#: ../../glossary/useful_information/surround_sound.rst:105
msgid ""
"To do this, the \"Music\" track can simply be duplicated with :menuselection:"
"`Edit --> Duplicate` after selecting it, and then :menuselection:`Split "
"Stereo to Mono` from the context menu of the third track. Then one of the "
"two mono tracks can be deleted; the other one can be renamed to \"LFE\"."
msgstr ""
"Om dit in orde te krijgen, kan u de \"Music\" track eenvoudig dupliceren "
"met :menuselection:`Edit --> Duplicate` nadat u het geselecteerd heeft, om "
"vervolgens :menuselection:`Splitsen Stereo naar Mono` in het contextmenu van "
"de derde track te selecteren. U kunt dan een van de twee mono tracks "
"verwijderen; de andere kan dan hernoemd worden naar \"LFE\"."

#: ../../glossary/useful_information/surround_sound.rst:108
msgid ""
"Now the \"Equalization...\" effect could be used to cut off frequencies "
"above around 100Hz from the \"LFE\" track, and reverse, cut off frequencies "
"below around 100Hz from the \"Music\" track."
msgstr ""
"Nu kunt u het **Equalization...**-effect gebruiken om de frequenties van "
"boven de 100Hz te verwijderen van de \"LFE\" track, en omgekeerd, de "
"frequenties van onder de 100Hz uit de \"Music\" track te verwijderen."

#: ../../glossary/useful_information/surround_sound.rst:113
msgid ""
"Creating technically perfect surround sound is a science all its own and "
"thus beyond the scope of this guide - please refer to respective resources "
"on the web for details."
msgstr ""
"De creatie van een technisch perfect surround sound is een wetenschap op "
"zich en valt daarom buiten de scope van deze handleiding - ga voor meer "
"details daarover naar de betreffende websites op het internet."

#: ../../glossary/useful_information/surround_sound.rst:116
msgid ""
"What remains for now is to make sure that the surround sound track has the "
"same length as the video track it should be added to. The video track used "
"in this example has a length of 1:00 minute, so the lengths of the audio "
"tracks in Audacity are adjusted accordingly:"
msgstr ""
"Wat voor nu overblijft is dat we moeten zorgen dat de surround sound track "
"dezelfde lengte heeft als de video track waaraan het later toegevoegd zou "
"moeten worden. De video track die in dit voorbeeld wordt gebruikt heeft een "
"lengte van 1:00 minuut, zodat in Audacity de lengtes van de audio tracks "
"dezelfde lengte krijgen:"

#: ../../glossary/useful_information/surround_sound.rst:119
msgid "The Audacity project should now look something like this:"
msgstr "Het Audacity-project zou er nu als volgt uit moeten zien::"

#: ../../glossary/useful_information/surround_sound.rst:125
msgid ""
"The next thing to do is to export the project to a multichannel 5.1 surround "
"sound audio file. The format used here is AC-3 (Dolby Digital)."
msgstr ""
"Het volgende wat we moeten doen is het exporteren van het project naar een  "
"multichannel 5.1 surround sound audiobestand. Het formaat wat hier wordt "
"gebruikt is AC-3 (Dolby Digital)."

#: ../../glossary/useful_information/surround_sound.rst:128
msgid ""
"Before exporting, Audacity needs to be configured to allow exporting to a "
"multichannel audio file: In :menuselection:`Edit --> Preferences`, under :"
"menuselection:`Import/Export`, select \"Use custom mix (for example to "
"export a 5.1 multichannel file)\"."
msgstr ""
"Voordat we kunnen exporteren, moet Audacity zodanig ingesteld worden dat het "
"kan exporteren naar een multichannel audiobestand: In :menuselection:`Edit --"
"> Preferences', in :menuselection:`Import/Export`, selecteert u \"Use custom "
"mix (for example to export a 5.1 multichannel file)\"."

#: ../../glossary/useful_information/surround_sound.rst:131
#: ../../glossary/useful_information/surround_sound.rst:319
msgid "The project can now be exported into a 5.1 surround sound audio file:"
msgstr ""
"Het project kan nu worden geëxporteerd naar een 5.1 surround sound "
"audiobestanden:"

#: ../../glossary/useful_information/surround_sound.rst:134
#: ../../glossary/useful_information/surround_sound.rst:322
msgid "Select :menuselection:`File --> Export...`"
msgstr "Selecteer :menuselection:`File --> Export...`"

#: ../../glossary/useful_information/surround_sound.rst:137
#: ../../glossary/useful_information/surround_sound.rst:324
msgid "Provide a name for \"Name\" and select \"AC3 Files (FFmpeg)\""
msgstr "Geef een naam op voor \"Name\" en selecteer \"AC3 Files (FFmpeg)\""

#: ../../glossary/useful_information/surround_sound.rst:140
msgid "Click :guilabel:`Options...` and choose \"512 kbps\" as \"Bit Rate\""
msgstr "Klik op :guilabel:`Options...` en kies \"512 kbps\" als \"Bit Rate\""

#: ../../glossary/useful_information/surround_sound.rst:143
msgid ""
"The \"Advanced Mixing Options\" dialog should show up. The number of "
"\"Output Channels\" should be 6 and the channel mapping should already be "
"correct:"
msgstr ""
"Het **Advanced Mixing Options** dialoogvenster zou nu moeten openen. Het "
"aantal **Output Channels** zou nu 6 moeten zijn en de koppelingen met de "
"kanalen zou al correct moeten zijn:"

#: ../../glossary/useful_information/surround_sound.rst:149
msgid ""
"The result of the export should be an :file:`*.ac3` file which is playable "
"with e.g. **VLC** or **Dragon Player**."
msgstr ""
"Het resultaat van de export zou een :file:`*.ac3`-bestand moeten zijn die "
"afspeelbaar moet zijn met b.v. **VLC** of **Dragon Player**."

#: ../../glossary/useful_information/surround_sound.rst:153
#: ../../glossary/useful_information/surround_sound.rst:339
msgid "Muxing Video and Audio Together"
msgstr "Het samen muxing van Video en Audio"

#: ../../glossary/useful_information/surround_sound.rst:155
msgid ""
"The final step is to add the surround sound audio track to the video track, "
"assuming the video was rendered without audio."
msgstr ""
"De laatste stap is om de surround sound audio track toe te voegen aan de "
"video track, aangenomen dat de video was rendered zonder audio."

#: ../../glossary/useful_information/surround_sound.rst:160
#: ../../glossary/useful_information/surround_sound.rst:347
msgid ""
"When muxing audio and video files into one file, the actual streams are just "
"copied, and not transcoded. So there is no quality loss to either the audio "
"or the video streams. Also, because the streams are just copied, muxing is "
"very fast."
msgstr ""
"Bij het muxxen van audio en video in een bestand, worden de eigenlijke "
"streams alleen maar gekopieerd en niet gehercodeerd. Zodat er geen "
"kwaliteitsverlies is bij zowel de audio als bij de video streams. Daarnaast, "
"omdat de streams alleen maar gekopieerd worden, is muxing erg snel."

#: ../../glossary/useful_information/surround_sound.rst:163
msgid ""
"Assuming the video track was rendered to :file:`Video.mkv` and the surround "
"sound was exported to :file:`5.1.ac3` the command to mux both to :file:"
"`Video-5.1.mkv` with **avconv** would be:"
msgstr ""
"Aangenomen dat de video track was rendered naar :file:`Video.mkv` en dat de "
"surround sound was geëxporteerd naar :file:`5.1.ac3` zal het commando om "
"zowel :file:`Video-5.1.mkv` en **avconv** samen te muxxen zijn:"

#: ../../glossary/useful_information/surround_sound.rst:171
#: ../../glossary/useful_information/surround_sound.rst:357
msgid ""
"The result should be an MKV video containing a Dolby Digital 5.1 surround "
"sound audio track."
msgstr ""
"Het resultaat zou moeten zijn een MKV video waarin is opgenomen een Dolby "
"Digital 5.1 surround sound audio track."

#: ../../glossary/useful_information/surround_sound.rst:175
msgid "Editing Existing Surround Sound"
msgstr "Bestaande Surround Sound bewerken"

#: ../../glossary/useful_information/surround_sound.rst:177
msgid ""
"When adding a clip with more than two channels to a project, **Kdenlive** "
"creates an audio thumbnail that correctly shows all audio channels:"
msgstr ""
"Bij het toevoegen van een clip met meer dan twee kanalen aan een project, "
"Zal **Kdenlive** een audio pictogram creëren die correct alle audio-kanalen "
"toont:"

#: ../../glossary/useful_information/surround_sound.rst:183
msgid ""
"The clip can be edited and (audio) effects applied to it, and all appears to "
"work just fine - but once rendering the project, it turns out that the audio "
"track in the resulting video file is 2 channels (stereo) only."
msgstr ""
"U kan de clip bewerken en er (audio) effecten op toepassen, en het lijkt "
"allemaal goed uit te zien - maar bij het renderen van het project, blijkt "
"dat de audio track in het resulterende videobestand alleen maar 2 kanalen "
"(stereo) heeft."

#: ../../glossary/useful_information/surround_sound.rst:186
msgid "The following steps provide a manual workaround for this issue."
msgstr "De volgende stappen geven een handmatige workaround voor dit probleem."

#: ../../glossary/useful_information/surround_sound.rst:190
msgid "Extract and Split the Audio Track"
msgstr "De Audio Track extraheren en splitsen"

#: ../../glossary/useful_information/surround_sound.rst:192
msgid ""
"The first step is to extract the audio track from the video clip. This can "
"be done in **Kdenlive** with :menuselection:`Extract Audio --> Wav 48000Hz` "
"from the context menu of the clip. This creates a WAV audio file in the same "
"folder as where the video clip is located."
msgstr ""
"De eerste stap is het extraheren van de audio track uit de video clip. Dit "
"kan u doen in **Kdenlive** met :menuselection:`Extract Audio --> Wav "
"48000Hz` in het contextmenu van de clip. Dit creëert een WAV audiobestand in "
"dezelfde map als waar de video clip is."

#: ../../glossary/useful_information/surround_sound.rst:195
msgid ""
"The extracted WAV audio file can then be opened in **Audacity**, it should "
"show all 6 channels, these are:"
msgstr ""
"Het geëxtraheerde WAV audiobestand kan u openen in **Audacity**, Het zou "
"alle 6 kanalen moeten tonen, dit zijn:"

#: ../../glossary/useful_information/surround_sound.rst:215
msgid ""
"The idea now is to split the surround sound into four separate (stereo/mono) "
"audio files that **Kdenlive** can handle:"
msgstr ""
"het idee is om nu de surround sound naar vier separate (stereo/mono) "
"audiobestanden te splitsen die **Kdenlive** wel kan hanteren:"

#: ../../glossary/useful_information/surround_sound.rst:217
#: ../../glossary/useful_information/surround_sound.rst:310
msgid "Front (stereo)"
msgstr "Front (stereo)"

#: ../../glossary/useful_information/surround_sound.rst:219
#: ../../glossary/useful_information/surround_sound.rst:312
msgid "Center (mono)"
msgstr "Center (mono)"

#: ../../glossary/useful_information/surround_sound.rst:221
#: ../../glossary/useful_information/surround_sound.rst:314
msgid "LFE (mono)"
msgstr "LFE (mono)"

#: ../../glossary/useful_information/surround_sound.rst:223
#: ../../glossary/useful_information/surround_sound.rst:316
msgid "Surround (stereo)"
msgstr "Surround (stereo)"

#: ../../glossary/useful_information/surround_sound.rst:225
msgid ""
"First, Audacity needs to be configured to not always export to stereo audio "
"files: In :menuselection:`Edit --> Preferences`, under :menuselection:"
"`Import/Export`, select \"Use custom mix (for example to export a 5.1 "
"multichannel file)\"."
msgstr ""
"Eerst moet Audacity zodanig ingesteld worden dat het niet altijd naar stereo "
"audiobestanden exporteert: In :menuselection:`Edit --> Preferences`, in :"
"menuselection:`Import/Export`, selecteert u \"Use custom mix (for example to "
"export a 5.1 multichannel file)\"."

#: ../../glossary/useful_information/surround_sound.rst:228
msgid ""
"Now, tracks 1+2 and 5+6 should be turned into stereo tracks by choosing :"
"menuselection:`Make Stereo Track` from the context menu of the 1st and the "
"5th track, respectively. This should result in 4 tracks, two stereo and two "
"mono."
msgstr ""
"Nu zouden de tracks 1+2 en 5+6 omgezet moeten zijn naar stereo tracks door "
"te selecteren :menuselection:`Make Stereo Track` in het contextmenu van "
"respectievelijk de 1ste en de 5de track. Dit zou moeten resulteren in 4 "
"tracks, twee stereo en twee mono."

#: ../../glossary/useful_information/surround_sound.rst:231
msgid ""
"Next, the 4 tracks should be renamed to \"Front\", \"Center\", \"LFE\" and "
"\"Surround\" starting from the top, using :menuselection:`Name...` from the "
"context menu of each track."
msgstr ""
"Als volgende stap moeten de 4 tracks hernoemt worden naar \"Front\", \"Center"
"\", \"LFE\" en \"Surround\" vanaf bovenaan beginnend, door in het "
"contextmenu van elke track te selecteren:`Name...`."

#: ../../glossary/useful_information/surround_sound.rst:234
msgid "The tracks now look like this:"
msgstr "De tracks zouden er nu als volgt uit moeten zien:"

#: ../../glossary/useful_information/surround_sound.rst:240
msgid ""
"After all this hard work, exporting the four tracks to four separate audio "
"files is easy with :menuselection:`File --> Export --> Export Multiple...`. "
"Use \"WAV\" as \"Export format\", the rest of the settings should already be "
"okay: \"Split files based on: Tracks\" and \"Name files: Using Label/Track "
"name\"."
msgstr ""
"Na al dit harde werk, is het exporteren van de vier tracks naar vier "
"separate audiobestanden makkelijk met :menuselection:`File --> Export --> "
"Export Multiple...`. Use \"WAV\" als \"Export format\", de rest van de "
"instellingen zou al in orde moeten zijn: \"Split files based on: Tracks\" en "
"\"Name files: Using Label/Track name\"."

#: ../../glossary/useful_information/surround_sound.rst:243
msgid ""
"The \"Edit metadata\" dialog might pop up for each track. It is fine to just "
"say :guilabel:`OK`. At the end there should be a confirmation dialog and "
"four audio files should have been exported: :file:`Front.wav`, :file:`Center."
"wav`, :file:`LFE.wav` and :file:`Surround.wav`."
msgstr ""
"Wellicht dat het dialoogvenster \"Edit metadata\" bij elke track opent. U "
"kunt hier gewoon op :guilabel:`OK` klikken. Aan het eind zou er een "
"bevestigings-dialoogvenster moeten komen waarna de vier audiobestanden "
"worden geëxporteerd: :file:`Front.wav`, :file:`Center.wav`, :file:`LFE.wav` "
"en :file:`Surround.wav`."

#: ../../glossary/useful_information/surround_sound.rst:247
msgid "Import Audio Tracks into Kdenlive"
msgstr "De Audio Tracks in Kdenlive importeren"

#: ../../glossary/useful_information/surround_sound.rst:249
msgid ""
"The previously created audio files can now be added to the Kdenlive project "
"using :menuselection:`Project --> Add Clip`."
msgstr ""
"De eerder gecreëerde audiobestanden kunnen nu aan het Kdenlive project "
"worden toegevoegd met :menuselection:`Project --> Clip toevoegen`."

#: ../../glossary/useful_information/surround_sound.rst:251
msgid ""
"Since there are only two audio tracks in a project by default, it is "
"necessary to add two more using :menuselection:`Project --> Tracks --> "
"Insert Track` before adding the four audio tracks to the timeline."
msgstr ""
"Omdat er standaard maar twee audio tracks in een project zijn, is het "
"noodzakelijk om nog twee eraan toe te voegen door menuselection:`Project --> "
"Tracks --> Track invoegen` voordat we de vier audio tracks toevoegen aan de "
"tijdlijn."

#: ../../glossary/useful_information/surround_sound.rst:253
msgid ""
"The next thing to do is to group the four audio tracks with the video clip "
"by selecting all of them and then choosing :menuselection:`Timeline --> "
"Group Clips`."
msgstr ""
"Het volgende wat we moeten doen is de vier audio tracks met de video clip te "
"groeperen door ze allemaal te selecteren en vervolgens :menuselection:"
"`Tijdlijn --> Clips groeperen` te kiezen."

#: ../../glossary/useful_information/surround_sound.rst:258
msgid ""
"Don't forget to mute the original audio track in the video clip if necessary!"
msgstr ""
"Vergeet niet om zo nodig de originele original audio track in de video clip "
"te muten!"

#: ../../glossary/useful_information/surround_sound.rst:261
msgid ""
"The **Kdenlive** project should now be ready for the usual editing, like "
"cutting clips and adding effects, and should look something like this:"
msgstr ""
"Het **Kdenlive** project zou nu gereed moeten zijn voor verdere handmatig "
"bewerken, zoals het knippen van clips en het toevoegen van effecten, en zou "
"er als volgt uit moeten zien:"

#: ../../glossary/useful_information/surround_sound.rst:268
msgid "Rendering the Project"
msgstr "Het project renderen"

#: ../../glossary/useful_information/surround_sound.rst:270
msgid ""
"Since it is not possible to render the project with a surround sound audio "
"track, some manual steps are necessary to work around this."
msgstr ""
"Omdat het niet mogelijk is om het project te renderen met een surround sound "
"audio track, zijn enkele handmatige stappen nodig om dit probleem te "
"overwinnen."

# "export audio"is niet te vinden in menu en niet programma
#: ../../glossary/useful_information/surround_sound.rst:272
msgid ""
"First, the video track needs to be rendered without audio. This is simply "
"done by rendering the project as it would normally be done, but without "
"audio, by deselecting the \"Export audio\" checkbox."
msgstr ""
"Eerst moet de video track gerenderd worden zonder audio. Dit doet u "
"eenvoudig door het project zoals u normaal zou doen te renderen, maar dan "
"zonder audio, door het keuzevakje \"Export audio\" te deselecteren."

#: ../../glossary/useful_information/surround_sound.rst:274
msgid ""
"Then, each of the four surround sound audio tracks :file:`Front.wav`, :file:"
"`Center.wav`, :file:`LFE.wav` and :file:`Surround.wav` needs to be rendered "
"into a separate audio file. For each of them, do the following:"
msgstr ""
"Daarna moet elk van de vier surround sound audio tracks :file:`Front.wav`, :"
"file:`Center.wav`, :file:`LFE.wav` en :file:`Surround.wav` naar een separaat "
"audiobestand gerenderd worden. Doe voor elk daarvan het volgende:"

#: ../../glossary/useful_information/surround_sound.rst:277
msgid "Mute all other audio tracks"
msgstr "Mute alle andere audio tracks"

#: ../../glossary/useful_information/surround_sound.rst:279
msgid "Enter a respective file name for \"Output file\""
msgstr "Voer een respectievelijke bestandsnaam in bij \"Output bestand\""

#: ../../glossary/useful_information/surround_sound.rst:281
msgid "Select :guilabel:`Audio only` as \"Destination\""
msgstr "Selecteer :guilabel:`Audio only` als \"Formaat\""

#: ../../glossary/useful_information/surround_sound.rst:283
msgid "Select profile \"WAV 48000 KHz\""
msgstr "Selecteer als profiel \"WAV 48000 KHz\""

#: ../../glossary/useful_information/surround_sound.rst:285
msgid "Make sure :guilabel:`Export audio` is checked"
msgstr "Controleer dat :guilabel:`Geluid exporteren` is ingeschakeld"

#: ../../glossary/useful_information/surround_sound.rst:292
msgid ""
"Unfortunately, the mono tracks :file:`Center.wav` and :file:`LFE.wav` are "
"rendered as stereo tracks, and there seems to be no way to avoid this. But "
"this can be handled later in Audacity."
msgstr ""
"Helaas worden de mono tracks :file:`Center.wav` en :file:`LFE.wav` gerenderd "
"als stereo tracks, en er lijkt geen manier te zijn om dit te vermijden. Maar "
"dit kan later in Audacity opgelost worden."

#: ../../glossary/useful_information/surround_sound.rst:296
msgid "Compose a Surround Sound Audio File"
msgstr "Stel nu een Surround Sound Audiobestand samen"

#: ../../glossary/useful_information/surround_sound.rst:298
msgid ""
"Now the separate audio tracks rendered by **Kdenlive** need to be \"merged\" "
"into a single multichannel 5.1 surround sound audio file. This is again done "
"in Audacity:"
msgstr ""
"Nu moeten de audio tracks die separaat door **Kdenlive** gerenderd zijn, "
"worden \"samengevoegd\" in een enkel multichannel 5.1 surround sound "
"audiobestand. Ook dit doet u in Audacity:"

#: ../../glossary/useful_information/surround_sound.rst:301
msgid ""
"Import :file:`Front.wav`, :file:`Center.wav`, :file:`LFE.wav` and :file:"
"`Surround.wav` (in this order!) using :menuselection:`File --> Import --> "
"Audio...`"
msgstr ""
"Importeer :file:`Front.wav`, :file:`Center.wav`, :file:`LFE.wav` en :file:"
"`Surround.wav` (in deze volgorde!) door gebruik te maken van :menuselection:"
"`File --> Import --> Audio...`"

#: ../../glossary/useful_information/surround_sound.rst:304
msgid ""
"\"Center\" and \"LFE\" are now stereo, which is not what is needed. This can "
"be fixed by selecting :menuselection:`Split Stereo to Mono` from the context "
"menu of each track, and deleting one of the two resulting mono tracks."
msgstr ""
"\"Center\" en \"LFE\" zijn nu stereo, en dat is niet wat nodig is. Dit kan "
"in orde gebracht worden door in het contextmenu van elke track :"
"menuselection:`Split Stereo to Mono` in het contextmenu te selecteren, en "
"dan een van de twee resulterende mono tracks te verwijderen."

#: ../../glossary/useful_information/surround_sound.rst:307
msgid "Eventually, there should be four tracks in the Audacity project:"
msgstr ""
"Uiteindelijk zouden er vier tracks in het Audacity project moeten zijn:"

#: ../../glossary/useful_information/surround_sound.rst:326
msgid ""
"Click :guilabel:`Options...` and choose :guilabel:`512 kbps` as \"Bit Rate\""
msgstr ""
"Klik op :guilabel:`Options...` en selecteer :guilabel:`512 kbps` as \"Bit "
"Rate\""

#: ../../glossary/useful_information/surround_sound.rst:329
msgid ""
"The **Advanced Mixing Options** dialog should show up. The number of "
"**Output Channels** should be 6 and the channel mapping should already be "
"correct:"
msgstr ""
"Het **Advanced Mixing Options** dialoogvenster zou nu moeten openen. Het "
"aantal **Output Channels** zou nu 6 moeten zijn en de koppelingen met de "
"kanalen zou al correct moeten zijn:"

#: ../../glossary/useful_information/surround_sound.rst:335
msgid ""
"The result of the export should be an :file:`*.ac3` file which is playable "
"with i.e. **VLC** or **Dragon Player**."
msgstr ""
"Het resultaat van de export zou een :file:`*.ac3`-bestand moeten zijn die "
"afspeelbaar moet zijn in b.v. **VLC** of **Dragon Player**."

#: ../../glossary/useful_information/surround_sound.rst:342
msgid ""
"Since video and audio was rendered separately, both need to be multiplexed "
"into a single file containing both the video and audio stream."
msgstr ""
"Omdat de video en de audio separaat zijn gerenderd, moeten beide naar een "
"enkel bestand worden gemultiplexed waarin zowel de video als audio in "
"voorkomen."

#: ../../glossary/useful_information/surround_sound.rst:350
msgid ""
"Assuming the video track was rendered to :file:`Video.mkv` and the surround "
"sound was exported to :file:`5.1.ac3`, the command to mux both to :file:"
"`Video-5.1.mkv` with **avconv** would be:"
msgstr ""
"Aangenomen dat de video track was gerendered naar :file:`Video.mkv` en dat "
"de surround sound was geëxporteerd naar :file:`5.1.ac3`, zal het commando om "
"beide te muxen naar :file:`Video-5.1.mkv` met **avconv** moeten zijn:"
