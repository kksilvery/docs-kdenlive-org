# Translation of docs_kdenlive_org_get_involved.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-16 11:51+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../get_involved.rst:1
msgid "Contribute to this Kdenlive video editor manual"
msgstr "Col·laborar amb aquest manual de l'editor de vídeo Kdenlive"

#: ../../get_involved.rst:1
msgid ""
"KDE, Kdenlive, Contribute, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, col·laboració, documentació, manual d'usuari, editor de "
"vídeo, codi lliure, lliure, aprendre, fàcil"

#: ../../get_involved.rst:15
msgid "Get involved"
msgstr "Com participar"

#: ../../get_involved.rst:17
msgid ""
"Contribute to this Manual. https://community.kde.org/Kdenlive/Workgroup/"
"Documentation"
msgstr ""
"Col·laboreu amb aquest Manual. https://community.kde.org/Kdenlive/Workgroup/"
"Documentation"
