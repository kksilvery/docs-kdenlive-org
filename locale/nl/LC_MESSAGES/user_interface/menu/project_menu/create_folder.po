# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 00:42+0000\n"
"PO-Revision-Date: 2022-01-02 14:42+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/project_menu/create_folder.rst:16
msgid "Create Folder"
msgstr "Map aanmaken"

#: ../../user_interface/menu/project_menu/create_folder.rst:18
msgid "Contents"
msgstr "Inhoud"

#: ../../user_interface/menu/project_menu/create_folder.rst:20
msgid ""
"There are three ways to access a menu containing this option: from the :ref:"
"`project_menu` menu, the :ref:`clips` dropdown on :ref:`the Project Bin "
"toolbar <project_tree>` or by right-clicking on a clip in the :ref:`Project "
"Bin <project_tree>`. This menu item creates a folder in the Project Bin. It "
"is a virtual folder, not one created on your hard disk.  You can use this "
"feature to organize your Project Bin when it gets very large or complex by "
"placing clips in folders, which can then be collapsed to free up space in "
"the tree. Existing clips in the Project Bin can be moved to a folder using "
"drag and drop. New clips can be added directly to a folder by first "
"selecting the folder (or any clip in the folder) and then choosing the :"
"menuselection:`Add Clip` option from one of the dropdown menus described "
"above."
msgstr ""
"Er zijn drie manieren om toegang te krijgen tot een menu met deze optie: uit "
"het menu :ref:`project_menu`, de afrol :ref:`clips` op de werkbalk :ref:`the "
"Project Bin toolbar <project_tree>` of door rechts te klikken op een clip in "
"de :ref:`Project Bin <project_tree>`. Dit menu-item maakt een map in de "
"Project-bin. Het is een virtuele map, niet een gemaakt op uw vaste schijf.  "
"U kunt deze functie gebruiken om uw Project-bin te organiseren wanneer het "
"erg groot of complex wordt door clips in mappen te plaatsen, die dan "
"ingevouwen kan worden om ruimte in de boomstructuur te maken. Bestaande "
"clips in de Project-bin kunnen verplaatst worden naar een map met slepen en "
"loslaten. Nieuwe clips kunnen direct toegevoegd worden aan een map door ze "
"eerst de map te selecteren (of een clip in de map) en dan de optie :"
"menuselection:`Clip toevoegen` te kiezen uit een van de afrolmenu's "
"bovenstaand beschreven."

#: ../../user_interface/menu/project_menu/create_folder.rst:25
msgid ""
"Edit the name of the folder: select the folder and right-click on the text "
"**Folder** or press :kbd:`F2`."
msgstr ""
"Bewerk de naam van de map: selecteer de map en klik rechts op de tekst "
"**Map** of druk op :kbd:`F2`."

#: ../../user_interface/menu/project_menu/create_folder.rst:28
msgid "Create additional bins"
msgstr "Extra bins aanmaken"

#: ../../user_interface/menu/project_menu/create_folder.rst:32
msgid ""
"You can create a separate bin from each folder, following :ref:`these steps "
"<multibin>`."
msgstr ""
"U kunt een aparte bin uit iedere map aanmaken, volgens :ref:`these steps "
"<multibin>`."
